import {Link} from "react-router-dom";

const HomeComponent = () => {

    return (
        <div>
            <Link to="/region">Region</Link>
            <br/>
            <Link to="/district">District</Link>
        </div>
    )
}
export default HomeComponent;