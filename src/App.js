import {BrowserRouter, Routes, Route} from "react-router-dom";
import Home from "./pages/home";
import Region from "./pages/region";
import District from "./pages/district";
import NoPage from "./pages/noPage";

const App = () => {
    return (
        <BrowserRouter>
            <Routes>
                <Route path="/" element={<Home/>}/>
                <Route path="/region" element={<Region/>}/>
                <Route path="/district" element={<District/>}/>
                <Route path="*" element={<NoPage/>}/>
            </Routes>
        </BrowserRouter>
    );
}

export default App;
