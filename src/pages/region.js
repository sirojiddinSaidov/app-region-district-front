import {
    Button,
    Col,
    Container,
    Form,
    FormGroup,
    Input, Label,
    Modal,
    ModalBody,
    ModalFooter,
    ModalHeader, Pagination, PaginationItem, PaginationLink,
    Row,
    Table
} from "reactstrap";
import axios from "axios";
import {useEffect, useState} from "react";

const Region = () => {
    const [formModalIsOpen, setFormModalIsOpen] = useState(false);
    const [name, setName] = useState('');
    const [selectedRegion, setSelectedRegion] = useState({});
    const [deleteModalIsOpen, setDeleteModalIsOpen] = useState(false);
    const [data, setData] = useState({});
    const [pages, setPages] = useState([]);
    const [sortType, setSortType] = useState('ASC');
    const [search, setSearch] = useState('');

    useEffect(() => {
        getRegions()
    }, [])

    const getRegions = (page, sort, input, bla) => {
        // if (!page || page !== data.number) {
        let url = 'http://localhost:8080/region?search=' + (bla ? input : search);
        if (page && sort) {
            let type = (sortType === 'ASC' ? 'DESC' : 'ASC');
            url += 'page=' + page + "&sort=" + sort + "&sortType=" + type;
            setSortType(type)
        } else if (page) {
            url += 'page=' + page;
        } else if (sort) {
            let type = (sortType === 'ASC' ? 'DESC' : 'ASC');
            url += "sort=" + sort + "&sortType=" + type;
            setSortType(type)
        }

        axios.get(url)
            .then(javob => {
                setData(javob.data);
                let totalPages = javob.data.totalPages;

                let arr = [];

                for (let i = 0; i < totalPages; i++)
                    arr[i] = i + 1;

                setPages(arr)

            })
            .catch(xato => console.log(xato));
    }

    const saveRegion = () => {
        if (selectedRegion.id) {
            axios.put(
                'http://localhost:8080/region/' + selectedRegion.id,
                {"name": name}).then(res => {
                setFormModalIsOpen(false);
                getRegions();
            })
        } else {
            axios.post('http://localhost:8080/region',
                {"name": name}).then(res => {
                setFormModalIsOpen(false);
                getRegions()
                // getRegions();
            }).catch(err => console.log(err))
        }
    }

    const getValueFromInput = (e) => {
        setName(e.target.value)
    }

    const openEditModal = (val) => {
        setSelectedRegion(val)
        setFormModalIsOpen(true)
    }
    const openDeleteModal = (val) => {
        setSelectedRegion(val);
        setDeleteModalIsOpen(true)
    }

    const deleteRegion = () => {
        axios.delete('http://localhost:8080/region/' + selectedRegion.id)
            .then(res => {
                setDeleteModalIsOpen(false)
                getRegions()
            })
    }

    const changeSearch = (e) => {
        setSearch(e.target.value);
        getRegions(0, '', e.target.value, true);
    }

    return (
        <div>
            <Container>
                <Row className="mt-5">
                    <Col><Button
                        color="success"
                        onClick={() => {
                            setFormModalIsOpen(true);
                            setSelectedRegion({})
                        }}>+ADD</Button></Col>
                    <Col>
                        <Input
                            placeholder="Search"
                            defaultValue={search}
                            onChange={changeSearch}/>
                    </Col>
                </Row>
                <Table hover>
                    <thead>
                    <tr>
                        <th>Tr</th>
                        <th
                            onClick={() => getRegions(data.number, 'name')}
                            style={{cursor: 'pointer'}}>Name
                        </th>
                        <th>District count</th>
                        <th colSpan={2}>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    {data.content?.map((region, i) =>
                        <tr key={region.id}>
                            <th>{i + 1}</th>
                            <td>{region.name}</td>
                            <td>{region.districts?.length}</td>
                            <td><Button color="warning" onClick={() => openEditModal(region)}>Edit</Button></td>
                            <td><Button color="danger" onClick={() => openDeleteModal(region)}>Delete</Button></td>
                        </tr>
                    )}
                    </tbody>
                </Table>


                <Pagination aria-label="Page navigation example">
                    <PaginationItem disabled={data.number === 0}
                                    onClick={() => getRegions(0)}>
                        <PaginationLink
                            first
                            href="#"
                        />
                    </PaginationItem>
                    <PaginationItem disabled={data.number === 0}
                                    onClick={() => getRegions(data.number - 1)}
                    >
                        <PaginationLink
                            href="#"
                            previous
                        />
                    </PaginationItem>


                    {pages.map((num, i) =>
                        <PaginationItem
                            active={i === data.number}
                            onClick={() => getRegions(i)}>
                            <PaginationLink href="#">
                                {num}
                            </PaginationLink>
                        </PaginationItem>
                    )}

                    <PaginationItem
                        onClick={() => getRegions(data.number + 1)}
                        disabled={data.number + 1 === data.totalPages}
                    >
                        <PaginationLink
                            href="#"
                            next
                        />
                    </PaginationItem>
                    <PaginationItem
                        onClick={() => getRegions(data.totalPages - 1)}
                        disabled={data.number + 1 === data.totalPages}>
                        <PaginationLink
                            href="#"
                            last
                        />
                    </PaginationItem>
                </Pagination>

            </Container>

            <Modal isOpen={formModalIsOpen}
                   toggle={() => setFormModalIsOpen(false)}>
                <ModalHeader>
                    Region {selectedRegion.id ? " tahrirlash" : " qo'shish"}
                </ModalHeader>
                <ModalBody>
                    <Label for="ketmonName">Name</Label>
                    <Input
                        onChange={getValueFromInput}
                        id="ketmonName"
                        defaultValue={selectedRegion.name}
                        name="ketmon"/>
                </ModalBody>
                <ModalFooter>
                    <Button color="danger"
                            type="button"
                            onClick={() => setFormModalIsOpen(false)}>Bekor qilish</Button>
                    <Button color="success" onClick={saveRegion}>Saqlash</Button>
                </ModalFooter>
            </Modal>

            <Modal isOpen={deleteModalIsOpen} toggle={() => setDeleteModalIsOpen(false)}>
                <ModalHeader>
                    Rostdan ham {selectedRegion.name} ni o'chirmoqchimisiz?
                </ModalHeader>
                <ModalFooter>
                    <Button color="danger"
                            onClick={() => setDeleteModalIsOpen(false)}>Bekor qilish</Button>
                    <Button color="success" onClick={deleteRegion}>O'chrish</Button>
                </ModalFooter>
            </Modal>
        </div>
    )
}

export default Region;